# Changelog

## UNRELEASED

## [v0.14.1] 2025-01-24
### Changed
- Use prettier date format for error messages.
- Update XEP-0474 to version 0.4.0 (requires go-xmpp >= 0.2.10).

## [v0.14.0] 2024-12-27
### Added
- Add `--fast-invalidate` to allow invalidating the FAST token.

### Changed
- Don't create legacy Ox private key directory in `~/.local/share/go-sendxmpp/oxprivkeys`.
- Delete legacy Ox private key directory if it's empty.
- Show proper error if saved FAST mechanism isn't usable with current TLS version (requires go-xmpp >= 0.2.9).
- Print debug output to stdout, not stderr (requires go-xmpp >= 0.2.9).
- Show `RECV:` and `SEND:` prefix for debug output (requires go-xmpp >= 0.2.9).
- Delete stored fast token if `--fast-invalidate` and `--fast-off` are set.
- Show error when FAST creds are stored but non-FAST mechanism is requested.

## [v0.13.0] 2024-12-14
### Added
- Add `--anonymous` to support anonymous authentication (requires go-xmpp >= 0.2.8).
- Add `XEP-0480: SASL Upgrade Tasks` support (requires go-xmpp >= 0.2.8).
- Add support for `see-other-host` stream error (requires go-xmpp >= 0.2.8).

### Changed
- Don't automatically try other auth mechanisms if FAST authentication fails.

## [v0.12.1] 2024-11-17
### Changed
- Print error instead of quitting if a message of type error is received.
- Allow upload of multiple files.

### Added
- Add flag `--suppress-root-warning` to suppress the warning when go-sendxmpp is used by the root user.

## [v0.12.0] 2024-11-03
### Added
- Add possibility to look up direct TLS connection endpoint via hostmeta2 (requires xmppsrv >= 0.3.3).
- Add flag `--allow-plain` to allow PLAIN authentication (requires go-xmpp >= 0.2.5).

### Changed
- Disable PLAIN authentication per default.
- Disable PLAIN authentication after first use of a SCRAM auth mechanism (overrides `--allow-plain`) (requires go-xmpp >= 0.2.5).

## [v0.11.4] 2024-10-26
### Changed
- Fix bug in SCRAM-SHA-256-PLUS auth (via go-xmpp >= 0.2.4).

## [v0.11.3] 2024-10-25
### Added
- Add go-xmpp library version to `--version` output (requires go-xmpp >= 0.2.2).

### Changed
- Fix XEP-0474: SASL SCRAM Downgrade Protection hash calculation bug (via go-xmpp >= 0.2.3).
- [gocritic]: Improve code quality.

## [v0.11.2] 2024-09-17
### Changed
- Add Gopenpgp and Xmppsrv version to `--version` output (requires xmppsrv >= 0.3.2).
- Improve selection between StartTLS and DirectTLS.

## [v0.11.1] 2024-07-11
### Changed
- Fix Ox encryption in interactive mode (do not add the same recipient key to the keyring over and over again).
- Exit with error code if Ox encryption for one recipient fails.
- Improved handling of perl sendxmpp config files.

## [v0.11.0] 2024-05-29
### Changed
- Move private Ox key into JID folder in ~/.local/share/go-sendxmpp.
- Use `fmt.Errorf()` instead of `errors.New()` to create new error messages.

### Added
- Add new parameter `--subject`.
- Added flag `--fast-off` to disable XEP-0484: Fast Authentication Streamlining Tokens (requires go-xmpp >= 0.2.1).

## [v0.10.0] 2024-04-13
### Changed
- Fixed a race condition in receiving stanzas (requires go-xmpp >= v0.1.5).

### Added
- Add support for SASL2 and BIND2 (via go-xmpp >= v0.2.0).
- Add support for FAST authentication (via go-xmpp >= v0.2.0).
- Add a warning when run by the user *root*.

## [v0.9.0] 2024-03-28
### Changed
- Properly close stream if `Ctrl+C` is pressed in interactive mode.
- Properly close stream if `Ctrl+C` is pressed in listening mode.
- Print OS, architecture and go version for flag `--version`.
- Improve closing of connection (via go-xmpp v0.1.4).
- Don't send stanzas that exceed the size limit provided by XEP-0478 (requires go-xmpp >= v0.1.4).
- Fixed hanging forever in stream close if the server doesn't reply with a closing stream element (via go-xmpp >= v0.1.4).

### Added
- New command line flag `ssdp-off` to disable XEP-0474: SASL SCRAM Downgrade Protection (requires go-xmpp >= v0.1.4).

## [v0.8.4] 2024-03-09
### Changed
- Properly handle lost connection.
- Better compatibility with perl sendxmpp config files.
- Improve file name for private Ox keys.
- Improve fallback behavior if no SRV records are provided.
- Remove 100ms sleep before closing the connection. This should be no more needed since go-xmpp commit 9684a8ff690f0d75e284f8845696c5057926d276.
- Return an error if there is no answer to an IQ within 60s. 
- Check for errors after sending the auth message during SCRAM authentication (via go-xmpp v0.1.2).

## [v0.8.3] 2024-02-17
### Changed
- Use a human readable file name for private Ox keys.
- Fix specifying a message via command line flag `-m`.

## [v0.8.2] 2024-01-19
### Changed
- Fix an issue in look up of SRV records (via xmppsrv v0.2.6). Thx mtp.

## [v0.8.1] 2024-01-16
### Added
- Add support for `tls-server-end-point` channel binding (via go-xmpp commit 3f0cbac30767faa562ad198ee69f36055f5924bc).
- Add experimental support for SOCKS5 proxies using the `HTTP_PROXY` environment variable (requires go-xmpp commit 685570cbd85c31ea3b426bea34dd4af404aac8cf).

### Changed
- http-upload: Improve error handling.

## [v0.8.0] 2024-01-09
### Added
- Add new parameter `--scram-mech-pinning`.

### Changed
- Refuse to upload a file if upload slot doesn't provide https.
- Use XEP-0474 instead of SCRAM mechanism pinning to prevent downgrade attacks (requires go-xmpp commit 39f5b80375b6f6f266df37b4a4adcbeb606ffec2).

## [v0.7.0] 2023-11-11
### Added
- Reply to XEP-0092 software version requests.
- Add support for PLUS variants of SCRAM authentication mechanisms (requires go-xmpp commit 4c385a334c606e8bc387f0a3d4d84975802b3984).
- Add pinning of last used authentication mechanism if a SCRAM mechanism was used.

### Changed
- Print every stanza in a new line (requires go-xmpp commit 31c7eb6919b67b18e901dc45a8e5681040ea7f31).

## [v0.6.2] 2023-09-29
### Changed
- Properly close connection to server if ^C is pressed in interactive mode.
- Replace invalid characters by UTF8 replacement char.
- Add warning that there is no Ox support for messages of type headline.
- Suppress warnings about reading from closed connection if go-sendxmpp closes the connection before exiting.
- Remove unnecessary newlines after stanzas.
- Fix segfault when authentication fails due to invalid username or password.

## Removed
- Removed deprecated flag and config option `resource`.

## [v0.6.1] 2023-07-25
### Changed
- Properly close connection to server.

## [v0.6.0] 2023-07-02
### Added
- Add support for sending Out of Band Data (OOB).
- Add SCRAM-SHA-1, SCRAM-SHA-256 and SCRAM-SHA512 support (via go-xmpp commit bef3e54).
- Add support for messages of type headline.

### Changed
- Print `--help` output to stdout instead of stderr.
- Print sent stanzas when `--debug` is used (via go-xmpp commit 9129a110df1b).
- Allow JIDs without localpart.
- Use single quotes for attributes in stanzas created by github.com/beevik/etree (vial etree v1.1.4).
- Fix SRV lookup when the domain has a CNAME (via xmppsrv v0.2.5).
- Fall back to directTLS on port 5223 (instead of StartTLS on port 5222) if no SRV records are provided and `-t` is set.
- Remove trailing newlines in interactive mode.

## [v0.5.6] 2022-11-11
### Added
- Add short option `-h` for http-upload.
### Changed
- Fix Ox key generation.

## [v0.5.5] 2022-10-16
### Changed
- Improve interactive mode.

## [v0.5.4] 2022-09-30
### Changed
- Fix http-upload.

## [v0.5.3] 2022-09-29
### Changed
- Don't check for empty message when `--listen` is used.

## [v0.5.2] 2022-09-29
### Added
- Reply to `disco#info` queries.
- Send `service-unavailable` errors for all other IQs of type `get` and `set`.
- New config option and command line flag `alias` to uncouple MUC nickname/alias from resource.

### Changed
- Ox: Improve error messages for failed key requests.
- Ox: Do not encrypt empty messages.
- Check for empty messages.
- Always look up CNAME before doing SRV lookups (via xmppsrv v0.2.3).
- Detect CNAME loops (max. 5 CNAMEs) (via xmppsrv >= v0.2.4).
- Deprecate resource config option and command line flag.
- Improve error handling in XML parsing.
- Don't connect to the XMPP server if the message is empty.

## [v0.5.1] 2022-05-22
### Changed
- Improve listening mode for groupchats.
- Fix nickname in MUCs if not manually set.

## [v0.5.0] 2022-05-05
### Added
- Add possibility to delete existing OpenPGP for XMPP nodes.

### Changed
- Ox: Improve rpad generation.
- Rework receiving of stanzas.
- Ox: Improve private key import.

## [v0.4.0] 2022-04-30
### Added
- Experimental support for Ox (OpenPGP for XMPP) encryption.

### Changed
- Shorter random ID for resource.
- Changed to the more flexible "github.com/beevik/etree" instead of encoding/xml for creating and interpreting stanzas.
- Changed to use message stamp, if provided, for printing the time stamp of received messages.

## [v0.3.0] 2022-03-21
### Added
- Added support for joining password protected MUCs.

### Changed
- Removed invalid code points from input.
- Fixed crash when reading a config with wrong syntax.
- Fixed crash when a non-existing or non-readable config was supplied by `-f`.
- Changed config file location from `~/.config/go-sendxmpp/sendxmpprc` to `~/.config/go-sendxmpp/config`.
- Fixed blocking of go-sendxmpp if an IQ reply of type "error" is received (via go-xmpp v0.0.0-20220319135856-e773596ea0b0).

## [v0.2.0] 2022-02-12
### Added
- Added listening function.
- Added flag to configure connection timeout.
- Added flag to configure minimum TLS version.
- Added flag to show version.

### Removed
- Removed deprecated option `-x`.

## [v0.1.3] 2022-01-29
### Changed
- Rename files to use a limited character set (alpha numerical and some extra characters) file name before uploading. Workaround for https://github.com/mattn/go-xmpp/issues/132

## [v0.1.2] 2021-11-18
### Changed
- Use xml.Marshal to safely build HTTP Upload request.
- Use salsa.debian.org/mdosch/xmppsrv for SRV lookups.

## [v0.1.1] 2021-09-12
### Changed
- Xml-escape file name in http-upload.
- Xml-escape mimetype in http-upload.

## [v0.1.0] 2021-09-11
### Added
- Initial release

module salsa.debian.org/mdosch/go-sendxmpp

go 1.23.0

toolchain go1.24.0

require (
	github.com/ProtonMail/gopenpgp/v2 v2.8.3
	github.com/beevik/etree v1.5.0
	github.com/gabriel-vasile/mimetype v1.4.8
	github.com/google/uuid v1.6.0
	github.com/pborman/getopt/v2 v2.1.0
	github.com/xmppo/go-xmpp v0.2.10
	golang.org/x/crypto v0.36.0
	salsa.debian.org/mdosch/xmppsrv v0.3.3
)

require (
	github.com/ProtonMail/go-crypto v1.1.6 // indirect
	github.com/ProtonMail/go-mime v0.0.0-20230322103455-7d82a3887f2f // indirect
	github.com/cloudflare/circl v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.37.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	golang.org/x/text v0.23.0 // indirect
)

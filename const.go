// Copyright Martin Dosch.
// Use of this source code is governed by the BSD-2-clause
// license that can be found in the LICENSE file.

package main

const (
	version = "0.14.2-dev"
	// defaults
	defaultBufferSize      = 100
	defaultConfigColumnSep = 2
	defaultDirRights       = 0o700
	defaultFileRights      = 0o600
	defaultFileRightsWin   = 0o200
	defaultIDBytes         = 12
	defaultLenServerConf   = 2
	defaultRpadMultiple    = 100
	defaultRSABits         = 4096
	defaultShortIDBytes    = 4
	defaultTimeout         = 10
	defaultTLSMinVersion   = 12
	defaultTLS10           = 10
	defaultTLS11           = 11
	defaultTLS12           = 12
	defaultTLS13           = 13
	// namespace
	nsC2SdTLS      = "urn:xmpp:alt-connections:tls"
	nsDiscoInfo    = "http://jabber.org/protocol/disco#info"
	nsDiscoItems   = "http://jabber.org/protocol/disco#items"
	nsEme          = "urn:xmpp:eme:0"
	nsHints        = "urn:xmpp:hints"
	nsHTTPUpload   = "urn:xmpp:http:upload:0"
	nsJabberClient = "jabber:client"
	nsJabberData   = "jabber:x:data"
	nsOx           = "urn:xmpp:openpgp:0"
	nsOxPubKeys    = "urn:xmpp:openpgp:0:public-keys"
	nsPubsub       = "http://jabber.org/protocol/pubsub"
	nsPubsubOwner  = "http://jabber.org/protocol/pubsub#owner"
	nsVersion      = "jabber:iq:version"
	nsXMPPStanzas  = "urn:ietf:params:xml:ns:xmpp-stanzas"
	// strings
	oxAltBody        = "This message is encrypted (XEP-0373: OpenPGP for XMPP)."
	pubsubPubOptions = "http://jabber.org/protocol/pubsub#publish-options"
	strChat          = "chat"
	strEmpty         = ""
	strError         = "error"
	strGroupchat     = "groupchat"
	strHeadline      = "headline"
	strResult        = "result"
)

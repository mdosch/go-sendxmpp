package main

import (
	"fmt"
	"os"
	"strings"
)

func findAuthPinFilePath(username string) (string, error) {
	dataPath, err := getDataPath(fsFriendlyJid(username), true)
	if err != nil {
		return strEmpty, fmt.Errorf("find auth pin file path: %v", err)
	}
	return fmt.Sprintf("%sauthpin", dataPath), nil
}

func parseAuthPinFile(username string) (bool, error) {
	// Find auth pin file.
	authPinFile, err := findAuthPinFilePath(username)
	if err != nil {
		return false, fmt.Errorf("parse auth pin file: %v", err)
	}
	// Read file.
	f, err := os.ReadFile(authPinFile)
	if err != nil {
		return false, fmt.Errorf("parse auth pin file: %v", err)
	}
	// Strip trailing newline.
	content := strings.TrimSuffix(string(f), "\n")
	noPLAIN := content == "noPLAIN"
	return noPLAIN, nil
}

func writeAuthPinFile(username string) error {
	// Find auth pin file.
	authPinFile, err := findAuthPinFilePath(username)
	if err != nil {
		return fmt.Errorf("write auth pin file: %v", err)
	}
	err = os.WriteFile(authPinFile, []byte("noPLAIN\n"), 0o400)
	return err
}
